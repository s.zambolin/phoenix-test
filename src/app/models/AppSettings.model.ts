export class AppSettings implements RootObject {
    homepage: Homepage[];
}

export interface Element {
    description: string;
    url: string;
    icon: string;
}

export interface Homepage {
    title: string;
    color: string;
    elements: Element[];
}

export interface RootObject {
    homepage: Homepage[];
}
