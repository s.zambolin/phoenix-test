import { AppSettings } from './../../../models/AppSettings.model';
import { AppSettingsService } from './../../../shared/services/app-settings.service';
import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  appSettings: AppSettings;

  constructor(private appSettingsService: AppSettingsService) { }


  ngOnInit() {
    this.appSettings = this.appSettingsService.getAll();
  }

}
