import { Element } from './../../../../models/AppSettings.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'riga',
  templateUrl: './riga.component.html',
  styleUrls: ['./riga.component.scss']
})
export class RigaComponent implements OnInit {

  @Input() dati: Element;
  constructor() { }

  ngOnInit() {
    console.log(this.dati)
  }

}
