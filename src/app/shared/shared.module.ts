import { AppSettingsService } from './services/app-settings.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AppSettingsService
  ],
  declarations: [],
  exports: [
  ]
})
export class SharedModule { }
