import { AppSettings } from './../../models/AppSettings.model';
import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class AppSettingsService {
  private appSettings: AppSettings = null;

  constructor(private http: Http) { }
  load() {
    return new Promise((resolve, reject) => {
      this.http.get('../../assets/appsettings.json')
        .map(res => res.json())
        .catch((error: any): any => {
          console.log('Configuration file "env.json" could not be read');
          resolve(true);
          return Observable.throw(error.json().error || 'Server error');
        })
        .subscribe((responseData: AppSettings) => {
          this.appSettings = <AppSettings>responseData;
          resolve(true);
      });
    }
    )
  }

  public getConfig(key: any): any {
    return this.appSettings[key]
  }
  public getAll(): AppSettings {
    return this.appSettings
  }


}
